import ast

class PatternVisitor(ast.NodeVisitor):

    def set_pattern(self, pattern):
        self.pattern = pattern

    def node_eval(self, node):
        if self.nodeEquals(node, pattern):
            print "Match to pattern found at line " + node.lineno

    def generic_visit(self, node):
        #print type(node).__name__
        ast.NodeVisitor.generic_visit(self, node)
        self.node_eval(node)

    def nodeEquals(self, a, b):
        result = isinstance(a, b.__class__)
        if result:
            op = " = "
        else:
            op = " != "
        print str(a.__class__.__name__) + op + str(b.__class__.__name__)

        if result:
            alist = []
            blist = []

            for i in ast.iter_child_nodes(a):
                alist.append(i)

            for j in ast.iter_child_nodes(b):
                blist.append(j)

            for k in range(len(alist)):
                if k < len(blist):
                    result = result and self.nodeEquals(alist[k], blist[k])
                else:
                    break

            return result

        else:
            for i in ast.iter_child_nodes(a):
                self.nodeEquals(i, b)


if __name__ == '__main__':
    pattern = ast.parse("print('hello')")
    #pattern = ast.parse('print a[i]')

    pattern_text = raw_input("Enter pattern sequence: ")
    pattern = ast.parse(pattern_text)

    filename = raw_input("Enter input file: ")

    reader = open(filename, 'r')
    text = ast.parse(reader.read())
    #text = ast.parse("a = 1; print('hello'); print('world'); print('hello')")

    print ast.dump(pattern)
    print ast.dump(text)

    patternArray = pattern.body
    textArray = text.body

    visit = PatternVisitor()
    for i in range(len(textArray)):
        match = True
        for j in range(len(patternArray)):
            if i + j < len(textArray):
                match = match and visit.nodeEquals(patternArray[j], textArray[i + j])
        if match:
            print "Match to pattern found at line " + str(textArray[i].lineno)

    #v = PatternVisitor()
    #v.set_pattern(pattern)
    #v.node_eval(text)
    #v.visit(text)